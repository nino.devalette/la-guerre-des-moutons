""" Version 0.1"""  

from random import randint

class Monde :
    """ constructeur : 
        _init_ : créer une matrice de dimension n 
                 créer une duree de repousse qui représente le temps que prend l'herbe pour repousser 
    operateurs :
        herbepousse : fait pousser une herbe sur toutes les cases de la matrice carré
        herbemangee : enleve une herbe sur une case
    accesseurs : 
        nbherbe : renvoie le nombre de case avec une herbe
        getCoefCarte : renvoie l'entier sur la case aux coordonnées i,j
    """
    def __init__(self,dimension,duree_repousse) :
        self.dimension = dimension
        self.duree_repousse = duree_repousse
        self.carte = [[0 for i in range(dimension)] for j in range(dimension)]
        for i in range(dimension) :
            for j in range(dimension) :
                self.carte[i][j] = randint(0,int(duree_repousse * 1.1))

    def herbepousse(self) :
        for i in range(self.dimension) :
            for j in range(self.dimension) :
                self.carte[i][j] += 1
   
    def herbemangee(self,i,j) :
        self.carte[i][j] = 0

    def nbherbe(self) :
        n = 0
        for i in range(self.dimension) :
            for j in range(self.dimension) :
                if self.carte[i][j] > self.duree_repousse :
                    n += 1
        return n
   
    def getCoefCarte(self,i,j) :
        return self.carte[i][j]

class Mouton :
    """ constructeur :
        _init_ : créer le gain d'énergie quand un mouton mange
                 place le mouton à la position x et y 
                 créer un entier qui représente l'énergie du mouton
                 créer un entier qui représente le taux de reproduction
    operateurs : 
        variationEnergie : diminue l'énergie d'un mouton se trouve sur une case ne contenant pas d'herbe ou au contraire augmente  l'énergie d'un mouton s'il se trouve sur une case contenant de l'herbe
        deplacement : déplace ou non le mouton aléatoirement sur l'une des huit cases adjacentes
        place_mouton : place un mouton aux coordonnées i et j
        """

    def __init__(self,x,y) :
        self.gain_nourriture = 4
        self.position_x = x
        self.position_y = y
        self.energie = 2 * self.gain_nourriture
        self.taux_reproduction = 4

    def variationEnergie(self,monde) :
        if monde.getCoefCarte(self.position_x,self.position_y) < monde.duree_repousse :
            self.energie -= 1
        else :
            self.energie += self.gain_nourriture
            monde.herbemangee(self.position_x,self.position_y)
   
    def deplacement(self,monde) :
        a = randint(0,8)
        if a == 0 and self.position_x < monde.dimension-1: self.position_x += 1
        elif a == 1 and self.position_x>0 : self.position_x -= 1
        elif a == 2 and self.position_y  < monde.dimension-1: self.position_y += 1
        elif a == 3 and self.position_y >0: self.position_y -= 1
        elif a == 4 and self.position_x < monde.dimension-1 and self.position_y < monde.dimension-1: self.position_x,self.      position_y = self.position_x + 1, self.position_y + 1
        elif a == 5 and self.position_x >0 and self.position_y < monde.dimension-1 : self.position_x,self.position_y = self.position_x - 1, self.position_y + 1
        elif a == 6 and self.position_x < monde.dimension-1 and self.position_y > 0: self.position_x,self.position_y = self.position_x + 1, self.position_y - 1
        elif a == 7 and self.position_x > 0 and self.position_y > 0: self.position_x,self.position_y = self.position_x - 1, self.position_y - 1
   
    def place_mouton(self,i,j) :
        self.position_x = i
        self.position_y = j
class simulation :

    def _init_(self,nombre_moutons,fin_du_monde,monde,max_mouton):
        self.nombre_moutons = nombre_moutons
        self.horloge = 0
        self.fin_du_monde = fin_du_monde
        self.moutons = [Mouton(randint(0,self.dimension-1), randint(0,self.dimension-1), self.monde.carte, i) for i in range(self.nombre_moutons)]
        self.monde = Monde
        self.resultat_herbe  = []
        self.resultat_mouton = []
        self.max_mouton = max_mouton
    
    def simMouton(self) : 

        """ def temps() : 
            while self.horloge < self.fin_du_monde :
                self.__horloge += 1
                self.monde.herbepousse()
        """

        for i in self.moutons:
            i.variationEnergie()
            if i.energie == 0:
                self.moutons.remove(i)
                self.monde.carte[i.position_y][i.position_x].remove('Mouton'+str(i.numMouton))
                continue 

            if self.monde.carte[i.position_y][i.position_x][0] == -1:
                    self.monde.herbeMangee(i.position_y, i.position_x)

        


        

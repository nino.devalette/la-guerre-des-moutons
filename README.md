# La Guerre des Moutons

La classe Monde : 
       Cette classe crée la carte sur laquelle évolue les moutons, les loups et les chasseurs.
       Les attributs de cette classe sont la dimension d'un coté de la carte, la duree pour qu'une herbe repounse et la carte. 
       Les méthodes de la classe Monde sont : - une méthode qui fait repousser l'herbe 
                                              - une méthode qui remet à 0 une case de la carte si un mouton a mangé sur cette case
                                              - une méthode qui renvoie le nombre de cases avec de l'herbe 
                                              - une méthode qui renvoie la valeur d'une case de la carte 

La classe Mouton :
        Cette classe affiche les données concernant les moutons. 
        Les attributs permettent de mettre en place: leur position, la nourriture qu'ils consomment, leur energie et le taux de reproduction de chaque mouton.
        Les méthodes de la classe Mouton se divise en trois parties. La première permet de suivre l'évolution de l'energie des moutons, en fonction des attributs sur la nourriture et sur l'energie. La seconde sert à placer un mouton à un endroit de la prairie, souvent utilisé lors de la naissance d'un mouton. La troisième est pour le deplacement, chaque mouton peut évolué aléatoirement dans les huits cases de la prairie. Cette dernière méthode sert également à gerer l'emplacement du mouton lorsqu'il sort de la zone.

La classe simulation : 
       Comme son nom l'indique, c'est cette classe qui gère toute les simulations. 
       Les attributs s'occupe de compter les moutons présent sur la carte, prend note du temps, gère la durée maximale de la simulation, fait appelle à la classe Monde,  genère une liste construite au fur et à mesure des nombres de case contenant de l'herbe et le nombre de moutons.
       Pour la classe simulation, il n'y a qu'une seule méthode qui gère la simulation de l'ensemble des attributs de cette classe et des autres classes précédentes. Il fait appelle à ses attributs, comme pour l'augmentation de l'horloge, lorsqu'il entreprend la sauvegarde des listes construites et les renvoient, lorsque la simulation doit prendre fin en prennant en compte de l'attribut horloge et fin du mondde.   Des autres classes pour faire pousser l'herbe, pour retirer un mouton lorsqu'il n'a plus d'énergie, pour la reproduction des moutons, et pour leur déplacement. 

